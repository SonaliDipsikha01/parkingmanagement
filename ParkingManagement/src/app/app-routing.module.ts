import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './elements/dashboard/dashboard.component';
import { ParkingLotComponent } from './elements/parking-lot/parking-lot.component';

const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'parkingLot', component: ParkingLotComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
