import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './elements/dashboard/dashboard.component';
import { ParkCarComponent } from './elements/park-car/park-car.component';
import { ParkingLotComponent } from './elements/parking-lot/parking-lot.component';
import { MaterialModule } from 'src/app/material/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StatusDialogComponent } from './elements/status-dialog/status-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    ParkCarComponent,
    ParkingLotComponent,
    StatusDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule, 
    ReactiveFormsModule,
    MaterialModule,
    BrowserAnimationsModule,

    
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents:[ParkCarComponent,StatusDialogComponent]
})
export class AppModule { }
