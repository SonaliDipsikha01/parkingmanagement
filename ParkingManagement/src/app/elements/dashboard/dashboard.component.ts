import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less']
})

export class DashboardComponent implements OnInit {
  public dashBoardForm: FormGroup;
  public showError: boolean = false;

  constructor(private _fb: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.dashBoardForm = this._fb.group({
      'parkingSlot': ['', [Validators.required]],
      'carParked': ['', Validators.required]
    });
  }

  /**
  *validationOfSlot -is to check validation of parking slot should be greater than parked car
  */
  validationOfSlot(event) {
    let parkingSlot = parseInt(this.dashBoardForm.get('parkingSlot').value);
    let parkedCar = parseInt(this.dashBoardForm.get('carParked').value);
    if (parkingSlot && parkedCar) {
      if (parkingSlot < parkedCar || parkedCar > 5) {//parked car should be less thna 6 as we have 5 hardcoded data
        this.showError = true
      }
      else {
        this.showError = false
      }
    }
    else
      this.showError=false

  }

  /**
  *onClear -will reset form
  */
  onClear() {
    this.dashBoardForm.reset();
  }

  /**
  *onSubmit-on form submit 
  *redirect to parkin lot page
  *and also store data in local storage
  */
  onSubmit() {
    this.clearLocalStorage()
    this.router.navigate(['/parkingLot']);
    window.localStorage.setItem("parkingDetails", JSON.stringify(this.dashBoardForm.value))
  }

  /**
  *onlyNumberKey- 
  *will take only numbers
  */
  onlyNumberKey(evt) {
    var ASCIICode = (evt.which) ? evt.which : evt.keyCode
    if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
      return false;
    return true;
  }
  
  clearLocalStorage(){
    if(window.localStorage.getItem('carDetails'))
    window.localStorage.removeItem('carDetails')
    if(window.localStorage.getItem('amount'))
    window.localStorage.removeItem('amount')
  }

}
