import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { DefaultParkedCars } from '../../config/constant'
import { FormBuilder, FormGroup } from '@angular/forms';
import { ParkCarComponent } from '../park-car/park-car.component';
import { MatDialog } from '@angular/material';
import { StatusDialogComponent } from '../status-dialog/status-dialog.component';

interface CarDetails {
  carNo: string,
  color: string,
  slotNo: number,
  dateTime: string
}

@Component({
  selector: 'app-parking-lot',
  templateUrl: './parking-lot.component.html',
  styleUrls: ['./parking-lot.component.less']
})
export class ParkingLotComponent implements OnInit {
  public dataSource = new MatTableDataSource([]);
  public actionForm: FormGroup;
  public displayedColumns: string[] = ['index', 'carNo', 'color', 'slotNo', 'dateTime', 'action'];
  public allData: CarDetails[] = [];
  public allFilterData: CarDetails[] = [];

  constructor(private _fb: FormBuilder, private dialog: MatDialog, private changeDetectorRefs: ChangeDetectorRef) { }

  public collectedMoney: number = 0;
  public noParkingSlot: number;
  public noParkedCar: number;
  colorFilterList: string[] = [];
  selected = -1;

  /*checkbox change event*/
  onChange(event) {
    console.log(event)
  }

  ngOnInit() {
    this.actionForm = this._fb.group({
      'carNo': [''],
      'color': ['']
    });
    this.getSlotDetails();
    this.addDefaultData();
    this.getCollectedMoney();
  }

  /**
   * getSlotDetails
   * @description will take slot details from local storage
   */
  getSlotDetails() {
    if (window.localStorage.getItem("parkingDetails")) {
      let slotDetails = JSON.parse(window.localStorage.getItem("parkingDetails"));
      this.noParkingSlot = parseInt(slotDetails["parkingSlot"]);
      this.noParkedCar = parseInt(slotDetails["carParked"]);
    }
  }

  /**
  * getCollectedMoney
  * @description will take collected amount details from local storage
  */
  getCollectedMoney() {
    if (!window.localStorage.getItem("amount")) {
      this.setAmountToLocalStorage(0);//if no data in local storage will store 0 to LS
    } else {
      this.collectedMoney = parseInt(window.localStorage.getItem("amount"));
    }
  }

  /**
  * addDefaultData
  * @description 
  *by default it will take hard coded data and also store to local storage
  *else on refresh will show updated page
  */
  addDefaultData() {
    if (window.localStorage.getItem('carDetails')) {
      this.allData = JSON.parse(window.localStorage.getItem('carDetails')).slice(0, this.noParkedCar)
    }
    else {
      this.allData = DefaultParkedCars.slice(0, this.noParkedCar);
    }
    this.allFilterData = JSON.parse(JSON.stringify(this.allData));
    this.dataSource.data = JSON.parse(JSON.stringify(this.allFilterData));
    this.getColorFilterList();
    this.setToLocalStorage(this.allData)
  }

  /**
  * getColorFilterList
  * @description 
  * rendered through all table data to take all possible color to make availbe to the filter
  */
  getColorFilterList() {
    this.colorFilterList=[]
    this.allData.forEach(data => {
      if (!this.colorFilterList.includes(data.color))
        this.colorFilterList.push(data.color);
    })
  }

  /**
    * updateColorFilterList
    * @description 
    * on add of new data update the filter color list as well
    */
  updateColorFilterList(color: string) {
    if (!this.colorFilterList.includes(color))
      this.colorFilterList.push(color);
  }

  /**
  * openDialog
  * @description 
  * on click of park car opens up dialo to fil up car details
  */
  openDialog() {
    const dialogRef = this.dialog.open(ParkCarComponent, {
      width: '720px',
      panelClass: 'ui-dialog'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.addParkingCar(result)
      }
    })
  }

  /**
    * addParkingCar
    * @description 
    * check for the avaiable slot and add data to table
    * if any new color added update the filter as well
    * if no slot avaiable upons up status dialog
    * @param car data
    */
  addParkingCar(data: CarDetails) {
    let availableSlot = this.getFreeSlot();
    if (availableSlot) {
      this.noParkedCar += 1;
      data["slotNo"] = availableSlot;
      this.allData.push(data);
      this.setToLocalStorage(this.allData);
      this.updateSlotDetails(this.noParkedCar);
      this.allFilterData = JSON.parse(JSON.stringify(this.allData));
      this.dataSource.data = JSON.parse(JSON.stringify(this.allFilterData));
      this.changeDetectorRefs.detectChanges();
      this.updateColorFilterList(data.color);
    }
    else {
      this.openStatusDialog()
    }
    this.resetFilter()
  }


  /**
  * updateSlotDetails
  * @description 
  * on add of new car update slot details in local storage as well
  */
  updateSlotDetails(parkedCar) {
    let data = JSON.parse(window.localStorage.getItem('parkingDetails'));
    data['carParked'] = parkedCar;
    window.localStorage.setItem('parkingDetails', JSON.stringify(data))
  }

  /**
  * openStatusDialog
  * @description 
  * if no slot avaialable shows mesage in status dialog
  */
  openStatusDialog() {
    const dialogRef = this.dialog.open(StatusDialogComponent, {
      width: '720px',
      panelClass: 'ui-dialog'
    });
  }

  /**
  * getFreeSlot
  * @description 
  * check with filled up slot to provide 1st emppty slot
  * @returns numbers(if slot available) else 0
  */
  getFreeSlot() {
    let allSlot = [];
    this.allData.forEach(data => {
      allSlot.push(data.slotNo)
    })
    for (var i = 1; i <= this.noParkingSlot; i++) {
      if (!allSlot.includes(i))
        return i
    }
    return 0
  }

  /**
   * setToLocalStorage
   * @description used to update data in local storage
   */
  setToLocalStorage(data) {
    if (data.length > 0)
      localStorage.setItem('carDetails', JSON.stringify(data));
    else
      localStorage.removeItem('carDetails');
  }

  /**
   * setAmountToLocalStorage
   * @description used to update amount in local storage
   */

  setAmountToLocalStorage(amount) {
    localStorage.setItem('amount', JSON.stringify(amount))
  }

  /**
   * removeCar
   * @description delete row and update local storage
   * and update collected monyey
   * update color filter
   * reset applied filter
   */
  removeCar(index: number) {
    this.noParkedCar -= 1;
    this.allData.splice(index, 1);
    this.allFilterData = JSON.parse(JSON.stringify(this.allData));
    this.dataSource.data = JSON.parse(JSON.stringify(this.allFilterData));
    this.setToLocalStorage(this.allData)
    this.changeDetectorRefs.detectChanges();
    this.collectedMoney += 20;
    this.setAmountToLocalStorage(this.collectedMoney);
    this.updateSlotDetails(this.noParkedCar);
    this.getColorFilterList();
    this.resetFilter()
  }

  /**
    * method to sort data of different individual column
    * direction can be asc or desc
    */
  sortData(event) {
    this.allData.sort(this.compareValues(event.active, event.direction));
    this.allFilterData = JSON.parse(JSON.stringify(this.allData));
    this.dataSource.data = JSON.parse(JSON.stringify(this.allFilterData));
  }

  /**
   *compareValues -is the sorting compare function
   *@param key -on which column sorting applied
   *@param order by default asc ,it can be asc or desc
   *@returns sorted data w.r.t single column
   */
  compareValues(key: string, order = 'asc') {
    return function innerSort(a: object, b: object) {
      if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
        // property doesn't exist on either object
        return 0;
      }

      const varA = (typeof a[key] === 'string')
        ? a[key].toLowerCase() : a[key];
      const varB = (typeof b[key] === 'string')
        ? b[key].toLowerCase() : b[key];

      let comparison = 0;
      if (varA > varB) {
        comparison = 1;
      } else if (varA < varB) {
        comparison = -1;
      }
      return (
        (order === 'desc') ? (comparison * (-1)) : comparison
      );
    };
  }

  /**filterOutCars
  * @description
  * filter out cars on table 
  */
  filterOutCars() {
    var data = JSON.parse(JSON.stringify(this.allData));
    if (this.actionForm.value) {
      for (var key in this.actionForm.value) {
        if (this.actionForm.value[key]) {
          data = this.searchTable(data, key, this.actionForm.value[key])
        }
      }
    }
    this.allFilterData = JSON.parse(JSON.stringify(data));
    this.dataSource.data = data;
  }

  searchTable(data, columnName, term: string) {
    return data.filter((item) => {
      return ((item[columnName].toString().toLowerCase().includes(term.toLowerCase())));
    });
  }


  /**resetFilter
   * @description reset applied filter
  */

  resetFilter() {
    this.actionForm.reset();
    this.allFilterData = JSON.parse(JSON.stringify(this.allData));
    this.dataSource.data = JSON.parse(JSON.stringify(this.allData));
  }
}


