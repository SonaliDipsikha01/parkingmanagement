import { Component, OnInit,ChangeDetectorRef } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import * as moment from 'moment';
@Component({
  selector: 'app-park-car',
  templateUrl: './park-car.component.html',
  styleUrls: ['./park-car.component.less']
})
export class ParkCarComponent implements OnInit {
  public colors=["BLUE","GREEN","BLACK","PINK","RED"]
  public carEntryForm: FormGroup;
  constructor(private _formBuilder: FormBuilder, public dialogRef: MatDialogRef<ParkCarComponent>) { }

  ngOnInit() {
    this.initiateForm();
  }

  /**
  * @description intiate form intiate form group with controller and validation
  */
  initiateForm():void {
    this.carEntryForm = this._formBuilder.group({
      carNo: ['', Validators.required],
      color: ['', Validators.required],
    })
  }
  /**
   * formSubmit
   * @description on form submit pass form data to parent compoonent with data
   */
  formSubmit():void {
    this.carEntryForm.value['dateTime']=moment(new Date()).format("YYYY-MM-DD 	hh:mm A")
    this.dialogRef.close(this.carEntryForm.value)
  }
 
  /**
   * clearForm
   * @description reset form
   */
  clearForm() {
    this.carEntryForm.reset()
  }
}
