import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-status-dialog',
  templateUrl: './status-dialog.component.html',
  styleUrls: ['./status-dialog.component.less']
})
export class StatusDialogComponent {

  constructor(public dialogRef: MatDialogRef<StatusDialogComponent>) { }

  closeDialog(){
    this.dialogRef.close()
  }
}
